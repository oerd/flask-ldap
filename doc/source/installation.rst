************
Installation
************

Install the extension with one of the following commands:

.. code-block:: bash

   pip install Flask-LDAP

or download the source code from here and run command:

.. code-block:: bash

   python setup.py build
   python setup.py install
