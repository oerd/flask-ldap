.. Flask-LDAP documentation master file, created by
   sphinx-quickstart on Mon Dec 23 16:12:21 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Flask-LDAP's documentation!
======================================

**Flask-LDAP** is an extension to Flask that allows you to easily add LDAP based authentication to your website. It depends on Flask, python-ldap and Flask-PyMongo(optionally).

Contents:

.. toctree::
   :maxdepth: 2

   features.rst
   installation.rst
   setting it up.rst
   API.rst
   example app.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

